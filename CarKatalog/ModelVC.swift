//
//  ModelVC.swift
//  CarKatalog
//
//  Created by uros.rupar on 5/31/21.
//

import UIKit

class ModelVC: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    
    @IBOutlet weak var logoTop: UIImageView!
    var model = ""
    var modeli :[ModelAutomobila] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        logoTop.image = UIImage(named: model)
        
        modeli = ModelAutomobila.filterByModel(model,ModelAutomobila.parseFile())
        
        
        
        mycollection.collectionViewLayout = UICollectionViewFlowLayout()
    }
    @IBOutlet weak var mycollection: UICollectionView!
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return modeli.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ModelCell
        
        if (UIImage(named:  modeli[indexPath.row].Model) != nil)
         {
            cell.modelImage.image = UIImage(named: modeli[indexPath.row].Model)
         }
         else
         {
            cell.modelImage.image = UIImage(named: model)
         }
        
        cell.modelName.text =  modeli[indexPath.row].Model
        cell.modelCena.text = "cena: " + modeli[indexPath.row].Cena
        
        cell.layer.borderWidth = 3
        cell.layer.borderColor = UIColor.red.cgColor
        cell.layer.cornerRadius = 20
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = (view.frame.width - 50) / 2

        var height = width
        
        if UIScreen.main.bounds.height <= 700{
            height += 45
        }

        return CGSize(width: width, height: height)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "carDetails" {
            
            let controller = segue.destination as! CarDetails
            
            let cell = sender as! UICollectionViewCell
            
            let indexPath = self.mycollection!.indexPath(for: cell)
            let selectedData = modeli[(indexPath?.row)!]
            controller.title = modeli[(indexPath?.row)!].Model
            
            controller.selectedModel = selectedData
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}




