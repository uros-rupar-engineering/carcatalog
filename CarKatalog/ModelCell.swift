//
//  ModelCell.swift
//  CarKatalog
//
//  Created by uros.rupar on 5/31/21.
//

import UIKit

class ModelCell: UICollectionViewCell {
    
    @IBOutlet weak var modelImage: UIImageView!
    
    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var modelCena: UILabel!
}
