//
//  ModelAutomobila.swift
//  CarKatalog
//
//  Created by uros.rupar on 5/31/21.
//

import Foundation

struct MarkaModel:Hashable{
    var marka:String
    var model: String
    
    var hashValue: Int { return [marka, model].hashValue

    }
  
}
func ==(lhs: MarkaModel, rhs: MarkaModel) -> Bool {
    return lhs.marka == rhs.marka && lhs.model == rhs.model}

class ModelAutomobila{
    
    var Marka:String
    var Model:String
    var Motor:String
    var PaketOpreme:String
    var Cena:String
    var RasporedCilindara:String
    var BrojVentila:String
    var PrecnikHodKlipa:String
    var TipUbrizgavanja:String
    var SistemOtvaranjaVentila:String
    var Turbo:String
    var ZapreminaMotora:String
    var KW:String
    var KS:String
    var SnagaPriObrtajima:String
    var ObrtniMoment:String
    var ObrtniMomentPriObrtajima:String
    var StepenKompresije:String
    var TipMenjaca:String
    var BrojStepeniPrenosa:String
    var Pogon:String
    var Duzina:String
    var Sirina:String
    var Visina:String
    var MedjuosovinskoRastojanje:String
    var TezinaPraznogVozila:String
    var MaksimalnaDozvoljenaTezina:String
    var ZapreminaRezervoara:String
    var ZapreminaPrtljaznika:String
    var MaksZapreminaPrtljaznika:String
    var DozvoljenTovar:String
    var DozvoljenoOpterecenjeKrova:String
    var DozvoljenaTezinaPrikoliceBK:String
    var DozvoljenaTezinaPrikoliceSK12:String
    var DozvoljenaTezinaPrikoliceSK8:String
    var OpterecenjeKuke:String
    var RadijusOkretanja:String
    var TragTockovaNapred:String
    var TragTockovaNazad:String
    var MaksimalnaBrzina:String
    var Ubrzanje0_100:String
    var Ubrzanje0_200:String
    var Ubrzanje80_120FinalniStepen:String
    var ZaustavniPut100:String
    var VremeZa400m:String
    var PotrosnjaGrad:String
    var PotrosnjaVGrada:String
    var KombinovanaPotrosnja:String
    var EmisijaCO2:String
    var Katalizator:String
    var PrednjeKocnice:String
    var ZadnjeKocnice:String
    var DimenzijePneumatika:String
    var PrednjeVesanje:String
    var ZadnjeVesanje:String
    var PrednjeOpruge:String
    var ZadnjeOpruge:String
    var PrednjiStabilizator:String
    var ZadnjiStabilizator:String
    var GarancijaKorozija:String
    var GarancijaMotor:String
    var EuroNCAP:String
    var EuroNCAPZvezdice:String
    var Gorivo:String
    var BrojVrata:String
    var BrojSedista:String
    
    
    
   
    
  
   
    init(Marka: String, Model: String, Motor: String, PaketOpreme: String, Cena: String, RasporedCilindara: String, BrojVentila: String, PrecnikHodKlipa: String, TipUbrizgavanja: String, SistemOtvaranjaVentila: String, Turbo: String, ZapreminaMotora: String, KW: String, KS: String, SnagaPriObrtajima: String, ObrtniMoment: String, ObrtniMomentPriObrtajima: String, StepenKompresije: String, TipMenjaca: String, BrojStepeniPrenosa: String, Pogon: String, Duzina: String, Sirina: String, Visina: String, MedjuosovinskoRastojanje: String, TezinaPraznogVozila: String, MaksimalnaDozvoljenaTezina: String, ZapreminaRezervoara: String, ZapreminaPrtljaznika: String, MaksZapreminaPrtljaznika: String, DozvoljenTovar: String, DozvoljenoOpterecenjeKrova: String, DozvoljenaTezinaPrikoliceBK: String, DozvoljenaTezinaPrikoliceSK12: String, DozvoljenaTezinaPrikoliceSK8: String, OpterecenjeKuke: String, RadijusOkretanja: String, TragTockovaNapred: String, TragTockovaNazad: String, MaksimalnaBrzina: String, Ubrzanje0_100: String, Ubrzanje0_200: String, Ubrzanje80_120FinalniStepen: String, ZaustavniPut100: String, VremeZa400m: String, PotrosnjaGrad: String, PotrosnjaVGrada: String, KombinovanaPotrosnja: String, EmisijaCO2: String, Katalizator: String, PrednjeKocnice: String, ZadnjeKocnice: String, DimenzijePneumatika: String, PrednjeVesanje: String, ZadnjeVesanje: String, PrednjeOpruge: String, ZadnjeOpruge: String, PrednjiStabilizator: String, ZadnjiStabilizator: String, GarancijaKorozija: String, GarancijaMotor: String, EuroNCAP: String, EuroNCAPZvezdice: String, Gorivo: String, BrojVrata: String, BrojSedista: String) {
        self.Marka = Marka
        self.Model = Model
        self.Motor = Motor
        self.PaketOpreme = PaketOpreme
        self.Cena = Cena
        self.RasporedCilindara = RasporedCilindara
        self.BrojVentila = BrojVentila
        self.PrecnikHodKlipa = PrecnikHodKlipa
        self.TipUbrizgavanja = TipUbrizgavanja
        self.SistemOtvaranjaVentila = SistemOtvaranjaVentila
        self.Turbo = Turbo
        self.ZapreminaMotora = ZapreminaMotora
        self.KW = KW
        self.KS = KS
        self.SnagaPriObrtajima = SnagaPriObrtajima
        self.ObrtniMoment = ObrtniMoment
        self.ObrtniMomentPriObrtajima = ObrtniMomentPriObrtajima
        self.StepenKompresije = StepenKompresije
        self.TipMenjaca = TipMenjaca
        self.BrojStepeniPrenosa = BrojStepeniPrenosa
        self.Pogon = Pogon
        self.Duzina = Duzina
        self.Sirina = Sirina
        self.Visina = Visina
        self.MedjuosovinskoRastojanje = MedjuosovinskoRastojanje
        self.TezinaPraznogVozila = TezinaPraznogVozila
        self.MaksimalnaDozvoljenaTezina = MaksimalnaDozvoljenaTezina
        self.ZapreminaRezervoara = ZapreminaRezervoara
        self.ZapreminaPrtljaznika = ZapreminaPrtljaznika
        self.MaksZapreminaPrtljaznika = MaksZapreminaPrtljaznika
        self.DozvoljenTovar = DozvoljenTovar
        self.DozvoljenoOpterecenjeKrova = DozvoljenoOpterecenjeKrova
        self.DozvoljenaTezinaPrikoliceBK = DozvoljenaTezinaPrikoliceBK
        self.DozvoljenaTezinaPrikoliceSK12 = DozvoljenaTezinaPrikoliceSK12
        self.DozvoljenaTezinaPrikoliceSK8 = DozvoljenaTezinaPrikoliceSK8
        self.OpterecenjeKuke = OpterecenjeKuke
        self.RadijusOkretanja = RadijusOkretanja
        self.TragTockovaNapred = TragTockovaNapred
        self.TragTockovaNazad = TragTockovaNazad
        self.MaksimalnaBrzina = MaksimalnaBrzina
        self.Ubrzanje0_100 = Ubrzanje0_100
        self.Ubrzanje0_200 = Ubrzanje0_200
        self.Ubrzanje80_120FinalniStepen = Ubrzanje80_120FinalniStepen
        self.ZaustavniPut100 = ZaustavniPut100
        self.VremeZa400m = VremeZa400m
        self.PotrosnjaGrad = PotrosnjaGrad
        self.PotrosnjaVGrada = PotrosnjaVGrada
        self.KombinovanaPotrosnja = KombinovanaPotrosnja
        self.EmisijaCO2 = EmisijaCO2
        self.Katalizator = Katalizator
        self.PrednjeKocnice = PrednjeKocnice
        self.ZadnjeKocnice = ZadnjeKocnice
        self.DimenzijePneumatika = DimenzijePneumatika
        self.PrednjeVesanje = PrednjeVesanje
        self.ZadnjeVesanje = ZadnjeVesanje
        self.PrednjeOpruge = PrednjeOpruge
        self.ZadnjeOpruge = ZadnjeOpruge
        self.PrednjiStabilizator = PrednjiStabilizator
        self.ZadnjiStabilizator = ZadnjiStabilizator
        self.GarancijaKorozija = GarancijaKorozija
        self.GarancijaMotor = GarancijaMotor
        self.EuroNCAP = EuroNCAP
        self.EuroNCAPZvezdice = EuroNCAPZvezdice
        self.Gorivo = Gorivo
        self.BrojVrata = BrojVrata
        self.BrojSedista = BrojSedista
    }
    
    
    

    
    static func parseFile()->[ModelAutomobila]{
        
        let fileName = "model2"
        let fileXstension = "csv"
        
        var fileContent : String?
        
       
        
        var automobili :[ModelAutomobila] = []
        let path = Bundle.main.path(forResource: fileName, ofType: fileXstension)
        
        fileContent = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        if let fileContent = fileContent{
            
           
            
            let lineByLineArray = fileContent.split(separator: "\n")
            
            
            for row in lineByLineArray{
                let lineToArray = row.split(separator: ";")
                
                
                let Marka = String(lineToArray[0]).trimmingCharacters(in: .whitespacesAndNewlines)
                
                
            
               
                var Model = String(lineToArray[1])
                var Motor = String(lineToArray[2])
                var PaketOpreme = String(lineToArray[3])
                var Cena = String(lineToArray[4])
                var RasporedCilindara = String(lineToArray[5])
                var BrojVentila = String(lineToArray[6])
                var PrecnikHodKlipa = String(lineToArray[7])
                var TipUbrizgavanja = String(lineToArray[8])
                var SistemOtvaranjaVentila = String(lineToArray[9])
                var Turbo = String(lineToArray[10])
                var ZapreminaMotora = String(lineToArray[11])
                var KW = String(lineToArray[12])
                var KS = String(lineToArray[13])
                var SnagaPriObrtajima = String(lineToArray[14])
                var ObrtniMoment = String(lineToArray[15])
                var ObrtniMomentPriObrtajima = String(lineToArray[16])
                var StepenKompresije = String(lineToArray[17])
                var TipMenjaca = String(lineToArray[18])
                var BrojStepeniPrenosa = String(lineToArray[19])
                var Pogon = String(lineToArray[20])
                var Duzina = String(lineToArray[21])
                var Sirina = String(lineToArray[22])
                var Visina = String(lineToArray[23])
                var MedjuosovinskoRastojanje = String(lineToArray[24])
                var TezinaPraznogVozila = String(lineToArray[25])
                var MaksimalnaDozvoljenaTezina = String(lineToArray[26])
                var ZapreminaRezervoara = String(lineToArray[27])
                var ZapreminaPrtljaznika = String(lineToArray[28])
                var MaksZapreminaPrtljaznika = String(lineToArray[29])
                var DozvoljenTovar = String(lineToArray[30])
                var DozvoljenoOpterecenjeKrova = String(lineToArray[31])
                var DozvoljenaTezinaPrikoliceBK = String(lineToArray[32])
                var DozvoljenaTezinaPrikoliceSK12 = String(lineToArray[33])
                var DozvoljenaTezinaPrikoliceSK8 = String(lineToArray[34])
                var OpterecenjeKuke = String(lineToArray[35])
                var RadijusOkretanja = String(lineToArray[36])
                var TragTockovaNapred = String(lineToArray[37])
                var TragTockovaNazad = String(lineToArray[38])
                var MaksimalnaBrzina = String(lineToArray[39])
                var Ubrzanje0_100 = String(lineToArray[40])
                var Ubrzanje0_200 = String(lineToArray[41])
                var Ubrzanje80_120FinalniStepen = String(lineToArray[42])
                var ZaustavniPut100 = String(lineToArray[43])
                var VremeZa400m = String(lineToArray[44])
                var PotrosnjaGrad = String(lineToArray[45])
                var PotrosnjaVGrada = String(lineToArray[46])
                var KombinovanaPotrosnja = String(lineToArray[47])
                var EmisijaCO2 = String(lineToArray[48])
                var Katalizator = String(lineToArray[49])
                var PrednjeKocnice = String(lineToArray[50])
                var ZadnjeKocnice = String(lineToArray[51])
                var DimenzijePneumatika = String(lineToArray[52])
                var PrednjeVesanje = String(lineToArray[53])
                var ZadnjeVesanje = String(lineToArray[54])
                var PrednjeOpruge = String(lineToArray[55])
                var ZadnjeOpruge = String(lineToArray[56])
                var PrednjiStabilizator = String(lineToArray[57])
                var ZadnjiStabilizator = String(lineToArray[58])
                var GarancijaKorozija = String(lineToArray[59])
                var GarancijaMotor = String(lineToArray[60])
                var EuroNCAP = String(lineToArray[61])
                var EuroNCAPZvezdice = String(lineToArray[62])
                var Gorivo = String(lineToArray[63])
                var BrojVrata = String(lineToArray[64])
                var BrojSedista = String(lineToArray[65])
                
                
                let model :ModelAutomobila = ModelAutomobila(Marka: Marka, Model: Model, Motor: Motor, PaketOpreme: PaketOpreme, Cena: Cena, RasporedCilindara: RasporedCilindara, BrojVentila: BrojVentila, PrecnikHodKlipa: PrecnikHodKlipa, TipUbrizgavanja: TipUbrizgavanja, SistemOtvaranjaVentila: SistemOtvaranjaVentila, Turbo: Turbo, ZapreminaMotora: ZapreminaMotora, KW: KW, KS: KS, SnagaPriObrtajima: SnagaPriObrtajima, ObrtniMoment: ObrtniMoment, ObrtniMomentPriObrtajima: ObrtniMomentPriObrtajima, StepenKompresije: StepenKompresije, TipMenjaca: TipMenjaca, BrojStepeniPrenosa: BrojStepeniPrenosa, Pogon: Pogon, Duzina: Duzina, Sirina: Sirina, Visina: Visina, MedjuosovinskoRastojanje: MedjuosovinskoRastojanje, TezinaPraznogVozila: TezinaPraznogVozila, MaksimalnaDozvoljenaTezina: MaksimalnaDozvoljenaTezina, ZapreminaRezervoara: ZapreminaRezervoara, ZapreminaPrtljaznika: ZapreminaPrtljaznika, MaksZapreminaPrtljaznika: MaksZapreminaPrtljaznika, DozvoljenTovar: DozvoljenTovar, DozvoljenoOpterecenjeKrova: DozvoljenoOpterecenjeKrova, DozvoljenaTezinaPrikoliceBK: DozvoljenaTezinaPrikoliceBK, DozvoljenaTezinaPrikoliceSK12: DozvoljenaTezinaPrikoliceSK12, DozvoljenaTezinaPrikoliceSK8: DozvoljenaTezinaPrikoliceSK8, OpterecenjeKuke: OpterecenjeKuke, RadijusOkretanja: RadijusOkretanja, TragTockovaNapred: TragTockovaNapred, TragTockovaNazad: TragTockovaNazad, MaksimalnaBrzina: MaksimalnaBrzina, Ubrzanje0_100: Ubrzanje0_100, Ubrzanje0_200: Ubrzanje0_200, Ubrzanje80_120FinalniStepen: Ubrzanje80_120FinalniStepen, ZaustavniPut100: ZaustavniPut100, VremeZa400m: VremeZa400m, PotrosnjaGrad: PotrosnjaGrad, PotrosnjaVGrada: PotrosnjaVGrada, KombinovanaPotrosnja: KombinovanaPotrosnja, EmisijaCO2: EmisijaCO2, Katalizator: Katalizator, PrednjeKocnice: PrednjeKocnice, ZadnjeKocnice: ZadnjeKocnice, DimenzijePneumatika: ZadnjeKocnice, PrednjeVesanje: PrednjeVesanje, ZadnjeVesanje: ZadnjeVesanje, PrednjeOpruge: PrednjeOpruge, ZadnjeOpruge: ZadnjeOpruge, PrednjiStabilizator: PrednjiStabilizator, ZadnjiStabilizator: ZadnjiStabilizator, GarancijaKorozija: GarancijaKorozija, GarancijaMotor: GarancijaMotor, EuroNCAP: GarancijaKorozija, EuroNCAPZvezdice: EuroNCAPZvezdice, Gorivo: Gorivo, BrojVrata: BrojVrata, BrojSedista: BrojSedista)

            
                automobili.append(model)
            }
            
        }
        return automobili
    }
    
    static func filterByModel(_ marka :String,_ automobili:[ModelAutomobila])->[ModelAutomobila]{
        
        return automobili.filter{$0.Marka == marka}
    }
    
    
    
    
    static func distinctMarkAndModel() -> [MarkaModel]{
    
        var arr:[MarkaModel] = parseFile().map{(elem) in return MarkaModel(marka: elem.Marka, model: elem.Model)}
        
        var set:Set<MarkaModel> = Set(arr)
        
        arr = Array(set)
        
        return arr
    }
    
}
