//
//  CarDetails.swift
//  CarKatalog
//
//  Created by uros.rupar on 6/1/21.
//

import UIKit

class CarDetails: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    
    
    var carDetails :[String] = []
    var properties :[String] = []
    
    var numberOfShownCell = 10

    @IBOutlet weak var showMoreDetails: UIButton!
    
    
    
    
    @IBOutlet weak var myTable: UITableView!
    
    var selectedModel = ModelAutomobila(Marka:"", Model:"", Motor: "", PaketOpreme:"", Cena: "", RasporedCilindara: "", BrojVentila: "", PrecnikHodKlipa: "", TipUbrizgavanja: "", SistemOtvaranjaVentila: "", Turbo: "", ZapreminaMotora: "", KW: "", KS: "", SnagaPriObrtajima: "", ObrtniMoment: "", ObrtniMomentPriObrtajima: "", StepenKompresije: "", TipMenjaca: "", BrojStepeniPrenosa: "", Pogon: "", Duzina: "", Sirina: "", Visina: "", MedjuosovinskoRastojanje: "", TezinaPraznogVozila: "", MaksimalnaDozvoljenaTezina: "", ZapreminaRezervoara: "", ZapreminaPrtljaznika: "", MaksZapreminaPrtljaznika: "", DozvoljenTovar: "", DozvoljenoOpterecenjeKrova: "", DozvoljenaTezinaPrikoliceBK: "", DozvoljenaTezinaPrikoliceSK12: "", DozvoljenaTezinaPrikoliceSK8: "", OpterecenjeKuke: "", RadijusOkretanja: "", TragTockovaNapred: "", TragTockovaNazad: "", MaksimalnaBrzina: "", Ubrzanje0_100: "", Ubrzanje0_200: "", Ubrzanje80_120FinalniStepen: "", ZaustavniPut100: "", VremeZa400m: "", PotrosnjaGrad: "", PotrosnjaVGrada: "", KombinovanaPotrosnja: "", EmisijaCO2: "", Katalizator: "", PrednjeKocnice: "", ZadnjeKocnice: "", DimenzijePneumatika: "", PrednjeVesanje: "", ZadnjeVesanje: "", PrednjeOpruge: "", ZadnjeOpruge: "", PrednjiStabilizator: "", ZadnjiStabilizator: "", GarancijaKorozija: "", GarancijaMotor: "", EuroNCAP: "", EuroNCAPZvezdice: "", Gorivo: "", BrojVrata: "", BrojSedista: "")
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showMoreDetails.backgroundColor = .red
       
        showMoreDetails.setTitle("Show more details", for: .normal)
        // Do any additional setup after loading the view.
        
     
        
        if (UIImage(named:  selectedModel.Model) != nil)
         {
            model.image = UIImage(named:  selectedModel.Model)
         }
         else
         {
            model.image = UIImage(named:  selectedModel.Marka)
         }
        
         properties = [
            "Marka","Model","Motor","Paket Opreme","Cena","Raspored Cilindara","BrojVentila","Precnik Hod Klipa","Tip Ubrizgavanja","Sistem Otvaranja Ventila","Turbo","Zapremina Motora","KW","KS","Snaga PriObrtajima","ObrtniMoment","Obrtni Moment Pri Obrtajima","Stepen Kompresije","Tip Menjaca","Broj StepeniPrenosa","Pogon","Duzina","Sirina","Visina","Medjuosovinsko Rastojanje","Tezina PraznogVozila","MaksimalnaD ozvoljena Tezina","Zapremina Rezervoara","Zapremina Prtljaznika","Maks Zapremina Prtljaznika","Dozvoljen Tovar","Dozvoljeno Opterecenje Krova","Dozvoljena TezinaPrikoliceBK","Dozvoljena Tezina Prikolice SK12","Dozvoljena Tezina Prikolice SK8","Opterecenje Kuke","Radijus Okretanja","Trag Tockova Napred","Trag Tockova Nazad","Maksimalna Brzina","Ubrzanje0-100","Ubrzanje0-200","Ubrzanje80-120FinalniStepen","Zaustavni Put100","Vreme Za 400m","Potrosnja Grad","Potrosnja VGrada","KombinovanaPotrosnja","Emisija CO2","Katalizator","Prednje Kocnice","ZadnjeKocnice","Dimenzije Pneumatika","PrednjeVesanje","ZadnjeVesanje","Prednje Opruge","Zadnje Opruge","Prednji Stabilizator","Zadnji Stabilizator","Garancija Korozija","Garancija Motor","EuroNCAP","EuroNCAP Zvezdice","Gorivo","BrojVrata","BrojSedista"]
        
        carDetails = [
            selectedModel.Marka,selectedModel.Model,selectedModel.Motor,selectedModel.PaketOpreme,selectedModel.Cena,selectedModel.RasporedCilindara,selectedModel.BrojVentila,selectedModel.PrecnikHodKlipa,selectedModel.TipUbrizgavanja,selectedModel.SistemOtvaranjaVentila,selectedModel.Turbo,selectedModel.ZapreminaMotora,selectedModel.KW,selectedModel.KS,selectedModel.SnagaPriObrtajima,selectedModel.ObrtniMoment,selectedModel.ObrtniMomentPriObrtajima,selectedModel.StepenKompresije,selectedModel.TipMenjaca,selectedModel.BrojStepeniPrenosa,selectedModel.Pogon,selectedModel.Duzina,selectedModel.Sirina,selectedModel.Visina,selectedModel.MedjuosovinskoRastojanje,selectedModel.TezinaPraznogVozila,selectedModel.MaksimalnaDozvoljenaTezina,selectedModel.ZapreminaRezervoara,selectedModel.ZapreminaPrtljaznika,selectedModel.MaksZapreminaPrtljaznika,selectedModel.DozvoljenTovar,selectedModel.DozvoljenoOpterecenjeKrova,selectedModel.DozvoljenaTezinaPrikoliceBK,selectedModel.DozvoljenaTezinaPrikoliceSK12,selectedModel.DozvoljenaTezinaPrikoliceSK8,selectedModel.OpterecenjeKuke,selectedModel.RadijusOkretanja,selectedModel.TragTockovaNapred,selectedModel.TragTockovaNazad,selectedModel.MaksimalnaBrzina,selectedModel.Ubrzanje0_100,selectedModel.Ubrzanje0_200,selectedModel.Ubrzanje80_120FinalniStepen,selectedModel.ZaustavniPut100,selectedModel.VremeZa400m,selectedModel.PotrosnjaGrad,selectedModel.PotrosnjaVGrada,selectedModel.KombinovanaPotrosnja,selectedModel.EmisijaCO2,selectedModel.Katalizator,selectedModel.PrednjeKocnice,selectedModel.ZadnjeKocnice,selectedModel.DimenzijePneumatika,selectedModel.PrednjeVesanje,selectedModel.ZadnjeVesanje,selectedModel.PrednjeOpruge,selectedModel.ZadnjeOpruge,selectedModel.PrednjiStabilizator,selectedModel.ZadnjiStabilizator,selectedModel.GarancijaKorozija,selectedModel.GarancijaMotor,selectedModel.EuroNCAP,selectedModel.EuroNCAPZvezdice,selectedModel.Gorivo,selectedModel.BrojVrata,selectedModel.BrojSedista
        ]
        
        
    }
    
   
    
    @IBOutlet weak var model: UIImageView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfShownCell
    }
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CarDetailsCell
        
        cell.property.text = properties[indexPath.row]
        cell.value.text = carDetails[indexPath.row]
        
        return cell
    }
 
    @IBAction func showMoreDetail(_ sender: Any) {
        
        if numberOfShownCell == 10{
            numberOfShownCell = carDetails.count
            showMoreDetails.setTitle("Show less details", for: .normal)
            
            myTable.reloadData()
        }else{
            numberOfShownCell = 10
            
            showMoreDetails.setTitle("Show more details", for: .normal)
            myTable.reloadData()
        }
        
        
    }
    
}
