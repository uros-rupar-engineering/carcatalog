//
//  ViewController.swift
//  CarKatalog
//
//  Created by uros.rupar on 5/31/21.
//

import UIKit

class ViewController: UIViewController {

    let USERNAME = "username"
    let PASSWORD = "username"
    

    override func viewDidLoad() {
        super.viewDidLoad()

        if USERNAME == PASSWORD{
            performSegue(withIdentifier: "home", sender: self)
        }
        
        
        usernameTextField.backgroundColor = UIColor.clear
        
        passwordTextfield.backgroundColor = UIColor.clear
        
        usernameTextField.alpha = 0.5
        
        passwordTextfield.alpha = 0.5
        
        usernameTextField.layer.borderWidth = 3
        passwordTextfield.layer.borderWidth = 3
        
        usernameTextField.layer.borderColor = UIColor.yellow.cgColor
        passwordTextfield.layer.borderColor = UIColor.yellow.cgColor
        
  
        
        buttonLogin.layer.cornerRadius = 20
        

    }
    
    
    
   
    @IBOutlet weak var buttonLogin: UIButton!
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "home"{
            let controller = segue.destination as! HomeVC
            
            controller.title = "Home screen"
        }
    }
    

    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextfield: UITextField!

    @IBAction func login(_ sender: Any) {
        if usernameTextField.text == USERNAME,passwordTextfield.text == PASSWORD{
            performSegue(withIdentifier: "home", sender: self)
        }else{
            makeAlert("Greska", "Niste uneli dobro username ili password")
        }
    }
    
    func makeAlert(_ heading:String,_ description:String){
        let alert  = UIAlertController(title: heading, message: description, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    
 
    }
    


}

