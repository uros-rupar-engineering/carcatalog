//
//  MarkeVC.swift
//  CarKatalog
//
//  Created by uros.rupar on 5/31/21.
//

import UIKit

class MarkeVC: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate{


    
  
    var markeArray:[MarkaModel] = []
    
    var arrString:[String] = []
    
    var filteredStringarr:[String] = []
    
    var filteredMarkearray :[MarkaModel]!
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var coollectionView: UICollectionView!
    
    //collection view
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredStringarr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MarkeCell
        
        cell.MarkaIme.text = filteredStringarr[indexPath.row]
        cell.markaLogo.image = UIImage(named:  filteredStringarr[indexPath.row])
        cell.layer.borderWidth = 3
        cell.layer.borderColor = UIColor.blue.cgColor
        cell.layer.cornerRadius = 20
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "model"{
            let controller = segue.destination as! ModelVC
            let cell = sender as! UICollectionViewCell
            
            let indexPath = self.coollectionView!.indexPath(for: cell)
            let selectedData = filteredStringarr[(indexPath?.row)!]
            controller.model = selectedData
            controller.title = selectedData
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = (view.frame.width - 60) / 2

        let height = width

        return CGSize(width: width, height: height)
        
    }

        // MARK: - UICollectionViewDelegateFlowLayout

    override func viewDidLoad() {
        super.viewDidLoad()
        markeArray = ModelAutomobila.distinctMarkAndModel()
        
        arrString = makeDisticMarkaarray(markeArray)
      
        filteredStringarr = arrString
        
        filteredMarkearray = markeArray
        
        coollectionView.collectionViewLayout = UICollectionViewFlowLayout()        // Do any additional setup after loading the view.
    
    }
    
    func makeDisticMarkaarray(_ marke:[MarkaModel])->[String]{
        let markeDuplicat = marke.map{(markaModel)in return markaModel.marka}
        var markeSet : Set<String> = []
        
        for item in markeDuplicat{
            markeSet.insert(item)
        }
        
        return Array(markeSet)
    }
    

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        filteredMarkearray = []
    
        if searchBar.text == "" {
            filteredStringarr = arrString
        }else{
            
            filteredStringarr = []
        for item in markeArray{
            if (item.marka.lowercased().contains(searchBar.text!.lowercased()) ||
                    item.model.lowercased().contains(searchBar.text!.lowercased())),!filteredStringarr.contains(item.marka){
                filteredStringarr.append(item.marka)
            }
        }
        
        
        }
        coollectionView.reloadData()
    }
    
    
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
        searchBarSearchButtonClicked(searchBar)
        
    }
    
    
   
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            filteredStringarr = arrString
            coollectionView.reloadData()
        }
    }
    
    
    
}


